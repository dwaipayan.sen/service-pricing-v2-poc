const PORT = 8000
// const url = 'https://www.theguardian.com/uk'
const createDatasheetUrl = 'https://stg-calculator.ff.cimpress.io/api/v1/dataSources/datasheets'
const createCalcUrl = 'https://stg-calculator.ff.cimpress.io/api/v1/calculators'
const publishCalcUrl = 'https://stg-calculator.ff.cimpress.io/api/v1/calculators/{id}/versions'
const createPriceConfigUrl = 'https://stg-priceconfiguration.ff.cimpress.io/api/v1/priceConfigurations'
const publishPriceUrl = 'https://stg-priceconfiguration.ff.cimpress.io/api/v1/prices/{id}/versions'
const evaluatePriceUrl = 'https://stg-priceconfiguration.ff.cimpress.io/api/v1/priceEvaluations:evaluate'
const express = require('express')
const axios = require('axios')

const app = express()


const evaluatePriceBody = {
      "seller": "g2Ez5VaoZWoqU22XqPjTLU",
      "priceType": "manufacturingCost",
      "priceDomain": "product",
      "evaluationDate": "2021-09-30T08:00:53.2749109+00:00",
      "resources": [
        {
          "resourceKey": "509409c4-4cc1-46cd-9fe7-5326e06aa2d9",
          "resourceId": "CIM-12345",
          "resourceType": "product",
          "inputSelections": [
            {
              "scope": "Product",
              "name": "Quantity",
              "value": "10"
            },
            {
              "scope": "Product",
              "name": "Product Id",
              "value": "CIM-12345"
            },
            {
              "scope": "Product",
              "name": "Substrate",
              "value": "300 GSM"
            }
          ]
        }
      ],
      "isPersisted": true
    }

    const headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6Ik1qbENNemxCTnpneE1ETkJSVFpHTURFd09ETkRSalJGTlRSR04wTXpPRUpETnpORlFrUTROUSJ9.eyJodHRwczovL2NsYWltcy5jaW1wcmVzcy5pby9lbWFpbCI6IkR3YWlwYXlhbi5TZW5AY2ltcHJlc3MuY29tIiwiaHR0cHM6Ly9jbGFpbXMuY2ltcHJlc3MuaW8vY2Fub25pY2FsX2lkIjoiRHdhaXBheWFuLlNlbkBjaW1wcmVzcy5jb20iLCJodHRwczovL2NsYWltcy5jaW1wcmVzcy5pby9hY2NvdW50IjoiZzJFejVWYW9aV29xVTIyWHFQalRMVSIsImh0dHBzOi8vY2xhaW1zLmNpbXByZXNzLmlvL2NpbXByZXNzX2ludGVybmFsIjp0cnVlLCJodHRwczovL2NsYWltcy5jaW1wcmVzcy5pby90ZW5hbnRzIjpbImNpbXByZXNzIl0sImlzcyI6Imh0dHBzOi8vY2ltcHJlc3MuYXV0aDAuY29tLyIsInN1YiI6IndhYWR8eEVYS29yUHlrUmJWUTRiNUtTcXRRaUlaZmc3cFZVM3BFRU9fR1c2UlFuOCIsImF1ZCI6WyJodHRwczovL2FwaS5jaW1wcmVzcy5pby8iLCJodHRwczovL2NpbXByZXNzLmF1dGgwLmNvbS91c2VyaW5mbyJdLCJpYXQiOjE2MzM1OTQ3MjksImV4cCI6MTYzMzY4MTEyOSwiYXpwIjoiRzE3SGROZDAxZ0FQZmlTVjV1cGJXZGlEVW5BVThpczkiLCJzY29wZSI6Im9wZW5pZCBwcm9maWxlIGVtYWlsIn0.scF6Gp9MMnClpxIJ_xoi-A3epzzarHMkz1F0SC5W8ERz2QYBhle3G0lw-sXYnuqRLnaXQ3fJyzWs8eWD1dheMpZ7g3KMJ9YwMtxqJMm9LPAyQg7VYQqY7kmmv3efCA2YqTZK59uyLhiid3w1Gfrb2EKUPKkMmHgG452WEHITWapdlzTc_Lt7Puq54GC7rW7sOpcm3zShzT8yydJ2FM3CFahBCpNSbYkGYun9PoR31tqwVYaGMRLzJREeXtYddD7BbcDaf3A3ptxmgDx7aK5-b1T09_LgYfmkk83Uthioyjx4x0q3uMfyS6ck8uW6bD41UIYA5wRmB9Tytl8C-3R5nw'
      }

    const evalutatePrice = () => {
      axios.post(evaluatePriceUrl, evaluatePriceBody, {
          headers: headers
      })
          .then(response => {
              const html = response.data
              console.log(html)
              return html
          })
          .catch(err => console.log(err))
    }

    console.log(evalutatePrice())

app.get('/evaluatePrice', function (req, res) {
    console.log("In evaluatePrice")
    evalutatePrice()
    // res.send('GET request to evaluatePrice')
    res.send(evalutatePrice())
  })


app.listen(PORT, () => console.log(`App running on PORT ${PORT}`))